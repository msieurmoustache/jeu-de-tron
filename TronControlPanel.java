/**
 * Fichier TronControlPanel.java
 * Date: 28 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe TronControlPanel: Classe qui est un sous-classe de JPanel
 *  et qui sera le panneau de contrôle.  
 *    (où les étiquesttes, l'état de la partie, les descriptifs des touches,
 *        le nombre de parties gagnées par chacun, les boutons pause/joueur, lnouvelle partie
 *            et le dropbox des choix de partie seront.)
 */
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class TronControlPanel extends JPanel
{
	private Arene a; 
	int nbjoueurs; 
	private int prochainepartie;
	private int ms; 
	private TronPanel tp;
	public JLabel etatCourant;
	
	
	/**
	 * Constructeur TronControlPanel 
	 * Où les attributs sont initialisé à partir des informations de l'arène 
	 * pour ce qui est des labels. Le reste est par défaut.
	 * @param arene 
	 */
	public TronControlPanel(Arene arene, TronPanel tp) 
	{
		a = arene;
		ms = 1000;
		this.tp = tp;

		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		
		joueurs();
		dropbox();
		boutons();
		millisecondes();
		
		etatCourant = new JLabel("Debut de partie");
		add(etatCourant);
		
	}
	
	/**
	 * Méthode qui crée les labels des différents joueurs, humain ou ordinateur.
	 *
	 */	
	public void joueurs() 
	{
		nbjoueurs = (a.joueurs).length;
 		for (int i =0; i< nbjoueurs; i++) 
 		{
 			String s = "("+etatJoueur(a.joueurs[i])+") "+"Joueur#"+(i+1)+" (Victoires: "+tp.vJ[i]+")";
 			JLabel j = new JLabel(s);
 			j.setForeground(a.joueurs[i].getCouleur());
 			add(j);
 			add(new JLabel(controles(i,a.joueurs[i])));
 		}
 		
	}
	
	/**
	 * Méthode qui crée un JLabel en fonction de l'état courant de la partie
	 */
	public void etatCourant() 
	{
		remove(etatCourant);
		if(tp.isRunning()) 
		{
			etatCourant = new JLabel("Partie en cours");
		}
		else etatCourant = new JLabel("Partie en pause");
		
		add(etatCourant);
	}
	
	
	/**
	 * Méthode qui ajoute un comboBox pour sélectionner les paramètres de la partie suivante.
	 *
	 */
	public void dropbox() 
	{
		JPanel j = new JPanel();
		j.setLayout(new GridLayout(2,1));
		j.add(new JLabel("Prochaine partie"));
		String[] s = new String[3];
		s[0] = "2 humains";
		s[1] = "1 humain et 1 ordinateur";
		s[2] = "2 humains et 1 ordinateur";
		JComboBox combo = new JComboBox(s);
		
		combo.addActionListener (new ActionListener ()
		{
		    public void actionPerformed(ActionEvent e)
			{
		    	JComboBox combo = (JComboBox)e.getSource();
		    	String s = (String) combo.getSelectedItem();
		    	switch(s) 
		    	{
		    	case "2 humains": prochainepartie=0;
		    	break;
		    	case "1 humain et 1 ordinateur":prochainepartie=1;
		    	break;
		    	case "2 humains et 1 ordinateur":prochainepartie=2;
		    	break;
		    		
		    	}
		    }
		});
		
		j.add(combo, BorderLayout.WEST);
		j.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(j);
	}
	
	/**
	 * Méthode qui génère les JButton pour lancer une nouvelle partie et Arrêter/Résumer le cours
	 * de la partie.
	 *
	 */
	public void boutons() 
	{
		nouvellepartie();
		pause();
	}
	
	
	
	/**
	 * Méthode qui retourne "Humain" si le Joueur en entrer est une sous-classe de JouerHumain et 
	 * "Ordinateur" s'il ne l'est pas. Est utilisé pour la composition de JLabel.
	 * 
	 * @param j: Référence au joueur.
	 */
	
	public static String etatJoueur(Joueur j) 
	{
		if(j instanceof JoueurHumain) return "Humain";
		else return "Ordinateur";
	}
	
	/**
	 * Méthode qui crée le JButton pour lancer une nouvelle partie. 
	 *
	 */
	public void nouvellepartie() 
	{
		JButton j = new JButton("Nouvelle partie");
		j.addActionListener(new ActionListener() 
		{
			
			public void actionPerformed(ActionEvent e)
			{
				tp.newgame();
				System.out.println("Nouvelle partie");				 // Pour tester l'action, soit l'afficher sur la console
			}
		});
		add(j,BorderLayout.SOUTH);
	}
	
	/**
	 * Méthode qui crée le JButton pour Arrêter ou Résumer une partie. 
	 */
	public void pause() 
	{
		
		try
		{
			Image img = ImageIO.read(getClass().getResource("img/pause.png"));
			ImageIcon icon = new ImageIcon(img);

			JButton j = new JButton(icon);
			j.addActionListener(new ActionListener() 
				{
					public void actionPerformed(ActionEvent e) {
						tp.stop();
						System.out.println("Stop"); 				// Pour tester l'action, soit l'afficher sur la console
					}
				});
			add(j,BorderLayout.CENTER);
		} 
		catch (IOException e1) 
		{
			e1.printStackTrace();
		}
		
	}
	
	/**
	 * Méthode qui crée le JTextArea pour modifier le nb de millisecondes entre les pas.
	 * Le ActionListener change l'attribut ms de TronControlPanel si ce qui 
	 * est écrit peut être parser.
	 */
	public void millisecondes() 
	{
		JPanel m = new JPanel();
		m.add(new JLabel("Millisecondes entre chaque pas"));
		JTextArea ta = new JTextArea("100");
		ta.getDocument().addDocumentListener(new DocumentListener() {
			
			public void changedUpdate(DocumentEvent e) //@Override
			{
				Document d = e.getDocument();
				String s;
				try
				{
					s = d.getText(0, d.getLength()-1);
					ms = Integer.parseInt(s);
				} 
				catch (BadLocationException e1) 
				{									// Ne modifiera pas la duree, affichera un message au joueur 
				}
	        	
			}

			public void insertUpdate(DocumentEvent e)    //Override
			{
				Document d = e.getDocument();
				String s="";
				try
				{
					s = d.getText(0, d.getLength());
					ms = Integer.parseInt(s);
				} 
				catch (BadLocationException e1) 
				{     							 // Ne modifiera pas la duree, affichera un message au joueur
				} 
				catch (NumberFormatException e1) 
				{
					System.out.println(s);
				}
			}

			public void removeUpdate(DocumentEvent e) //@Override
			{
			}
	    });
		m.add(ta);
		m.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(m);
	}
	
	/**
	 *  Méthode qui retourne les commandes qu'un joueur doit utilisé pour jouer.
	 *
	 *  @param i: Le chiffre du joueur pour les commandes
	 *  @param j: le Joueur
	 */
	public String controles(int i, Joueur j) 
	{
		if(j instanceof JoueurHumain) {
			switch (i) 
			{
			case 0: return "Haut: W Droite: D Bas: S Gauche: D";
			case 1: return "Haut: I Droite: L Bas: K Gauche: J";
			default: return "";
			}
		}
		else return "";
	}
	
	/**
	*	Methodes qui retournent les attributs.
	*
	*/
	public int getMS() {return ms;}
	public int getParaNewGame() {return prochainepartie;}
	public int getNbJoueurs() {return nbjoueurs;}
	
	/**
	*	Méthode qui permet de le changement de tp.
	*
	*/	
	public void setTronPanel(TronPanel p) {tp = p;} 
	
}