import java.awt.*;
/**
 * Fichier JoueurHumain.java
 * Date: 28 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe JoueurHumain: Classe qui crée l'objet JoueurHumain qui est une sous-classe de Joueur.
 * Elle sert principalement à différentier les joueurs humains du joueurs ordinateur.
 *
 */
public class JoueurHumain extends Joueur 
{
    public JoueurHumain(Color c, int dim_h, int dim_larg) 
	{
        super(c, dim_h, dim_larg);
    }
  
}
