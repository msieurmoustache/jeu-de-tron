/**
 * Fichier Noeud.java
 * Date: 31 Mars 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe Noedu permet la création de la classe ListeChainee.
 * Elle crée une "cellule donnée" de la liste.
 */
public class Noeud
{
    /**
     * Contenu: L'information de la cellule de la liste soit l'objet.
     * Next: Le noeud suivant de la liste
     */
    private Object contenu;
    private Noeud next;

    /**
     * Constructeur de Noeud
     *  Initialise Contenu du Noeud.
     */
    public Noeud(Object info)
    {
        contenu = info;
    }

    /**
     * Méthodes qui permettent la modification des attributs de Noeud
     *
     */
    public void setNext(Noeud prochain)
    {
        next = prochain;
    }
    public void setContenu(Object info)
    {
        contenu = info;
    }

    /**
     * Méthode qui retournent les attributs de Noeud.
     */
    public Object getContenu()
    {
        return contenu;
    }
    public Noeud getNext()
    {
        return next;
    }

}
