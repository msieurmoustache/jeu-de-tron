import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Fichier Controles.java
 * Date: 28 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe Controles: Classe aidant à l'implentation des contrôles pour les joueurs. (Soit w,a,s,d et i,j,k,l)
 */
public class Controles implements KeyListener 
{
	
	public Arene a; 
	private JoueurHumain[] j; 
	
	/**
	*	Constructeur de Cotnroles
	* 	Initialise l'attribut Arène a 
	* 	 et lui prend tous ses joueurs humains pour initialiser le tableau j.
	*
	*/
	
	public Controles(Arene a) 
	{
		this.a = a;
		j = new JoueurHumain[2];
		int n =0;
		Joueur[] joueurs = a.getJoueur();
		for (int i=0; i<joueurs.length;i++) 
		{
			if(joueurs[i] instanceof JoueurHumain) {				
				j[n] = (JoueurHumain) joueurs[i];
				n++;
			}
		}
		}
	

	/**
	*	@Override
	*	Fait réagir les touches du clavier 
	*	et donne une nouvelle direction au joueur selon la touche appuyé.
	*
	*/
	public void keyTyped(KeyEvent e) {
		char c = e.getKeyChar();
		c = Character.toLowerCase(c);
		// System.out.println("Caractère appuyé: "+c);                                  // pour l'afficher sur la console
		switch (c) 
		{
			case 'w': 
				if( j[0].getDirection_courante() != "s" )	 j[0].setNouvelle_direction("n");           // En haut
				break;
			case 'i':
				if(j[1] != null && j[1].getDirection_courante() != "s")j[1].setNouvelle_direction("n");  // En haut
				break;																						
			case 'd': 
				if(j[0].getDirection_courante() != "o") j[0].setNouvelle_direction("e");                // À droite
				break;
			case 'l': 
				if(j[1] != null && j[1].getDirection_courante() != "o")j[1].setNouvelle_direction("e");  // À droite
				break;
			case 's': 
				if(j[0].getDirection_courante() != "n") j[0].setNouvelle_direction("s");                // En bas
				break;
			case 'k': 
				if(j[1] != null && j[1].getDirection_courante() != "n") j[1].setNouvelle_direction("s"); // En bas
				break;
			case 'a': 
				if(j[0].getDirection_courante() != "e") j[0].setNouvelle_direction("o"); 			    // À gauche
				break;
			case 'j': 
				if(j[1] != null && j[1].getDirection_courante() != "e") j[1].setNouvelle_direction("o"); // À gauche
				break;
		}
		
	}
	
	/**
	*	@Override
	* 	Méthodes nécessaire pour l'implentation de KeyListener
	*
	*/
	public void keyPressed(KeyEvent e) { }
	public void keyReleased(KeyEvent e){ }

}
