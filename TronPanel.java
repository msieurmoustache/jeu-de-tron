/**
 * Fichier TronPanel.java
 * Date: 28 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe TronPanel: Classe qui est une sous-classe de JPanel qui met en place l'arène et TronControlPanel
 *   et qui met aussi en place la logique du jeu.
 */
import java.awt.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;

public class TronPanel extends JPanel {
	private Arene a;
	private TronControlPanel tcp; 
	private boolean encours;
	public Timer timer; 
	private Tron tron;
	private JLabel cache; 
	private int joueursVivant;
	public int[] vJ;
	
	/**
	*	Constructeur de TronPanel
	*   Initialise les attributs 
	*
	*/
	public TronPanel() 
	{
		vJ = new int[3];
		for (int i = 0; i<vJ.length;i++)
		{
			vJ[i] = 0;
		}
		
		setFocusable(true);
		encours = true;
		a = new Arene(2, false);
		
		joueursVivant = (a.getJoueur()).length;
		a.addKeyListener(new Controles(a));
		a.requestFocus();
		
		tcp = new TronControlPanel(a,this);
		tcp.setTronPanel(this);
		tcp.setPreferredSize(new Dimension(280, 200));
		
		setLayout(new BorderLayout());
	
		add(tcp, BorderLayout.EAST);
		add(a, BorderLayout.CENTER);
		timer = new Timer(tcp.getMS(), new ActionListener()   //Iimer
		{			
			public void actionPerformed(ActionEvent e)		//@Override
			{
				timeraction();
				//System.out.println("timer");               //Pour afficher les "ticks" du timer sur la console
				revalidate();
				repaint();				
			}
		});
		timer.setRepeats(true);
		timer.start();
	}
	
	/**
	* Méthode qui indique ce que le timer doit faire à chaque fois
	*
	*/
	public void timeraction() 
	{
		for (int i = 0; i<a.getJoueur().length;i++ ) 
		{
			if ((a.getJoueur())[i].getVivant()) (a.getJoueur())[i].deplacement();
		}
		
		timer.setDelay(tcp.getMS());
		//Joueur.printTrace(a.getJoueur()[0]);
		//Joueur.printTrace(a.getJoueur()[1]); //Pour afficher la trace du joueur[0] sur la console.
		victoire();	
		
	}
	
	/** 
	*	Méthodes qui permettent d'arrêter et de repartir le timer
	*
	*/
	public void stop()
	{
		if(encours == true)	
		{
			timer.stop();
			remove(a);
			
			cache = new JLabel("Arene non-visible lors de la pause");
			add(cache, BorderLayout.CENTER);			
		}
		else 
		{
			if(cache != null) remove(cache);
			add(a, BorderLayout.CENTER);
			a.requestFocusInWindow();
			timer.start();
		}
		
		encours = !encours;
		tcp.etatCourant();
		revalidate();
		repaint();
		
	}
	

	/** 
	 *  Méthode qui vérifie les conditions de victoire
	 *
	 *  S'il ne reste qu'un joueur, celui-ci a une nouvelle vie et une nouvelle partie débute i.e, 
	 *  l'arene et la position des joueurs est reset et le nb de victoires est incremente.
	 */
	
	public void victoire() 
	{
		mort();
		if(joueursVivant == 1 ) 
		{
			for (int i = 0; i<a.getJoueur().length;i++) 
			{
				if(a.getJoueur()[i].getVivant()) 
				{
					vJ[i]++;
					timer.stop();
					tcp.remove(tcp.etatCourant);
					tcp.etatCourant = new JLabel("Joueur "+(i+1)+" a gagne. Nouvelle partie.");
					revalidate();
					tcp.add(tcp.etatCourant);
					repaint();
				}
			}
		}
		else if (joueursVivant < 1)
		{
			timer.stop();
			tcp.remove(tcp.etatCourant);
			tcp.etatCourant = new JLabel("Match nul. Nouvelle partie.");
			revalidate();
			tcp.add(tcp.etatCourant);
			repaint();
		}
		
	}
	
	/**
	 *  Méthode qui vérifie la mort des joueurs. 
	 *  Arrête la progression des joueurs morts,
	 *  Un joueur est mort si sa tete touche une trace.
	 *  
	 */
	public void mort() 
	{
		Joueur[] joueurs = a.getJoueur();
		for (int i = 0; i < joueurs.length ; i++) 
		{
			Point tete = (joueurs[i].getT()).tete();
			for (int j = 0; j < joueurs.length; j++) 
			{
				Trace t = joueurs[j].getT();
				if( t.contient(tete)&& joueurs[i].getVivant()) 
				{
					if (i == j) 
					{
						ListeChainee l = new ListeChainee();
						for (int k = 0; k<t.get_liste().size()-1; k++) 
						{
							l.append(t.get_liste().get(k));
						}
						t = new Trace(l);
						if (t.contient(tete)) 
						{
							joueurs[i].mort();
							joueursVivant--;
						}
					}
					else {
						joueurs[i].mort();
						joueursVivant--;
					}

					
				}
			}
			Trace t = a.enceinte;
			if( t.contient(tete)&& joueurs[i].getVivant() ) 
			{
				joueurs[i].mort();
				joueursVivant--;
			}
		}
	}
	
	/**
	* Méthode qui crée une nouvelle partie à partir des options sélectionnées par les joueurs
	*
	*/
	
	public void newgame() 
	{
		int p = tcp.getParaNewGame();
		switch(p) 
		{
		case 0: 
			a=new Arene(2,false);
			joueursVivant = 2;
			break;
		case 1:
			a=new Arene();
			joueursVivant = 2;
			break;
		case 2:
			a=new Arene(2,true);
			joueursVivant = 3;
			break;
		}
		removeAll();
		a.addKeyListener(new Controles(a));
		tcp = new TronControlPanel(a,this);
		tcp.addKeyListener(new Controles(a));
		tcp.setTronPanel(this);
		
		tcp.setPreferredSize(new Dimension(280, 200));
		add(tcp, BorderLayout.EAST);
		add(a, BorderLayout.CENTER);
		
		timer.setDelay(tcp.getMS());
		a.requestFocus();
		
		revalidate();
		repaint();
	
		
		
	}
	
		
	/** 
	*	Méthodes qui retournent les attributs voulus.
	*
	*/
	public boolean isRunning() {return encours;}
	public Arene getArene() {return a;}
	
	
}