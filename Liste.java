/**
 * Fichier Liste.java
 * Date: 31 Mars 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * L'Interface Liste.
 */
public interface Liste
{
    /**
     * Grandeur de la liste
     * @return Le nombre d'éléments de la liste
     */
    int size();

    /**
     * Ajoute un objet à la fin de la liste
     * Augmente le nombre d'élément de la liste
     * @param o: Objet à ajouter
     */
    void append(Object o);

    /**
     * Ajoute un objet au début de la liste.
     * Augmente le nombre d'élément de la liste.
     * @param o: Objet à ajouter
     */
    void preprend(Object o);

    /**
     * Retourne la référence du premier objet de la liste.
     * @return: Référence du premier objet
     */
    Object getFirst();

    /**
     * Retourne la référence du dernier objet de la liste.
     * @return: Référence du dernier objet.
     */
    Object getLast();

    /**
     * Retourne la référence du premier objet de la liste,
     *  l'enlève de la liste et diminue le nombre d'élément de la liste.
     * @return: Référence du premier objet
     */
    Object removeFirst();

    /**
     * Retourne une référence de l'objet à la position donnée.
     * @param position: Indice de la position de l'objet voulu.
     * @return: Référence de l'objet voulu.
     */
    Object get(int position);

    /**
     * Remplace l'objet à la position donnée par un autre objet.
     * @param position: Indice de la position de l'objet voulu.
     * @param o: Objet qui remplacera.
     */
    void set(int position, Object o);

}
