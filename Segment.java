/**
 * Fichier Segment.java
 * Date: 26 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe Segment: Classe qui crée l'objet segment avec l'aide de la classe Point.
 */

public class Segment
{
	private Point debut; 
	private Point fin; 
	private String orientation;
	
	/**
	* 	Constructeur de Segment.
	* 	Initialise le debut et la fin du segment.
	*
	*	@param debut: Point du début du segment.
	*	@param fin: Point de fin du segment.
	*/
	public Segment (Point debut, Point fin)
	{
		this.debut = debut;
		this.fin = fin;  
	}

	/**
	*	Autre constructeur de Segment
	*	qui ne fait qu'un segment avec qu'un point et donc l'orientation est voulu vers l'est.
	*
	*	@param start: Point de départ du segment.
	*
	*/
	public Segment(Point start)
	{
		debut = start; 
		fin = start;
		orientation = "e";
	}
	
	/**
	*	Autre Constructeur de Segment
	*	Permettant la création, l'augmentation et la diminution de celui selon son orientation.
	*
	*	@param start: point de début du segment
	*	@param o: String indiquant l'orientation du point de fin du segment.
	*/
	public Segment(Point start, String o)
	{
		debut = start; 
		switch(o) 
		{

			case "n": fin = new Point(start.getX(),start.getY()-1);
			break;
			case "s":fin = new Point(start.getX(),start.getY()+1);
			break;
			case "o":fin = new Point(start.getX()-1,start.getY());
			break;
			case "e":fin = new Point(start.getX()+1,start.getY());
			break;
			}
           
		orientation = o;
	}
  
  /**
  * Méthodes qui retournent ou permette le changement des attributs du segment.
  *
  *
  */
  public Point getDebut() {return debut;}
  public Point getFin() {return fin;}
  public String getOrientation() {return orientation;} 
  
  public void setDebut(Point debut) {this.debut=debut;}
  public void setFin(Point fin) {this.fin=fin;}
  public void setOrientation(String orientation) {this.orientation = orientation;}


  /**
  *	Override de la méthode toString.
  *	Aidant au débogage.
  */
  public String toString()
  {
    String str = "Mon point de début est "+debut.getX()+","+debut.getY()+" et ma tête est "+fin.getX()+","+fin.getY();
    return str;
  }
}