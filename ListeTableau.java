/**
 * Fichier ListeTableau.java
 * Date: 31 Mars 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe ListeTableau crée la structure de donnée: la liste en forme de tableau.
 */
public class ListeTableau implements Liste
{
    /**
     * n : Taille de liste.
     * tab: La liste tableau.
     */
    private int n; Object[] tab;

    /**
     * Constructeur de ListeTableau
     *  Initialise la taile de la liste et son tableau.
     */
    public ListeTableau()
    {
        tab = new Object[10];
        n = 0;
    }

    /**
     * Override de la méthode toString pour l'affichage de ListeTableau.
     * @return: String qui affichera les éléments de liste.
     */
    public String toString()
    {
        String s = "ListeTableau[";
        for (int i = 0; i<n; i++)
        {
            s+=tab[i]+",";
        }
        s = s.substring(0,s.length()-1);
        s+="]";
        return s;
    }

    /**
     * Méthode checkN()
     * S'assure que la grandeur du tableau contenant les objets de la liste
     * soit toujours suffisamment grand.
     *
     */
    public void checkN()
    {
        if (n == tab.length)
        {
            Object[] tab2 = tab;
            tab = new Object[n*2];
            for (int i =0; i<n; i++)
            {
                tab[i] = tab2[i];
            }
        }
      
    }

    /**
     *  Méthodes de l'interface
     *    Voir Liste.java
     */
    
    public int size()
    {
        return n;
    }

    public void append(Object o)
    {
        tab[n] = o;
        n++;
        checkN();
    }

    public void preprend(Object o)
    {
        checkN();
        for (int i = n; i>0;i--)
        {
            tab[i] = tab[i-1];
        }
        tab[0] = o;
        n++;
      
    }

    public Object getFirst()
    {
        if (n>0) return tab[0];
        else  throw new IndexOutOfBoundsException("Liste vide");
    }

    public Object getLast()
    {
        if (n>0) return tab[n-1];
        else  throw new IndexOutOfBoundsException("Liste vide");
    }

    public Object removeFirst()
    {
        Object temp = tab[0];
        n--;
        if (n<0) throw new IndexOutOfBoundsException("Position hors de la liste ");for (int i =0; i<n;i++) tab[i]=tab[i+1];
        tab[n] = null;
        return temp;
    }

    public Object get(int position)
    {
        if (position >= n | position<0)  throw new IndexOutOfBoundsException("Position hors de la liste ");
        return tab[position];
    }

    public void set(int position, Object o)
    {
        if (position >= n | position<0)  throw new IndexOutOfBoundsException("Position hors de la liste ");
        tab[position] = o;
    }
}