/**
 * Fichier Tron.java
 * Date: 28 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe Tron: Classe contenant la fonction main et permettant l'appartion de la fenêtre contenant le jeu.
 */

import javax.swing.*;
public class Tron {
	public static void main(String[] args)
	{
		JFrame j = new JFrame("Tron");
		TronPanel tp = new TronPanel();
		
		tp.addKeyListener(new Controles(tp.getArene()));
		
		j.add(tp);
		j.setSize(850,580);
	
		j.setLocationRelativeTo(null);
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setVisible(true);
	}
}
