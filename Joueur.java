/**
 * Fichier Joueur.java
 * Date: 28 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe Joueur: Classe qui crée l'objet joueur.
 */
import java.awt.Color;
import java.util.Random;

public class Joueur 
{
    protected Trace t;
    private boolean vivant;
    protected String direction_courante;
    protected String nouvelle_direction;
    private Color couleur;

	/**
	* 	Constructeur de Joueur:
	* 	Initialise la trace du joueur en lui choissisant un point de départ aléatoire.
	* 	Initialise la direction courante par défault soit vers l'est.
	* 	Initialise sa vitatlité à True (vivant)
	* 	Initialise la couleur qu'il aura
	*
	*	@param c: La couleur du joueur
	*	@param dim_larg_pt: Dimension de la largeur en point
	*	@param dim_h_pt: Dimension de la hauteur en point
	*/
    public Joueur(Color c, int dim_larg_pt, int dim_h_pt)
    {
        Random rand = new Random();
        Point depart = new Point(1+rand.nextInt(dim_larg_pt-2), 1+rand.nextInt(dim_h_pt-2));
        direction_courante = "e";
        nouvelle_direction = "e";
        t = new Trace(depart, direction_courante);

        vivant = true;
        couleur = c;

    }
	/**
	* 	Les prochaines méthodes retournent l'attribut demandé.
	*
	*/
    public Trace getT() 
	{
        return t;
    }
	public boolean getVivant() 
	{
        return vivant;
    }
	public String getDirection_courante()
	{
        return direction_courante;
    }
    public String getNouvelle_direction() 
	{
        return nouvelle_direction;
    }
    public Color getCouleur()
	{
        return couleur;
    }
	
	/**
	*	Méthode mort change le boolean vivant pour false, car le joueur est mort.
	*
	*/
    public void mort()
	{
        vivant = false;
    }
	/**
	*	Méthode SetNouvelle_direction permet de changer l'attribut Nouvelle_direction.
	*
	*/
	public void setNouvelle_direction(String n)
	{
        nouvelle_direction = n;
    }
   
	/**
	* 	Méthode deplacement s'occupe modifier la trace tout le long du jeu
	* 	il y a un changent de direction lorsque nouvelle_direction n'égale pas direction courante.
	*
	*
	*/
    public void deplacement() {
        if (nouvelle_direction == direction_courante) {
            t.allonge(direction_courante);
        } else {
            direction_courante = nouvelle_direction;
            t.allonge(direction_courante);
        }
    }
	
	/**
	* 	Méthode nécessaire pour assure_difference de la Classe Arène.
	*	Elle donne un nouveau point de départ à la trace du joueur.
	*
	*   @param dim_larg_pt: Dimension de la largeur en point
	*	@param dim_h_pt: Dimension de la hauteur en point
	*/
	public Trace nouvelle_Trace(int dim_larg_pt, int dim_h_pt) {
        Random rand = new Random();
        Point depart = new Point(rand.nextInt(dim_larg_pt), rand.nextInt(dim_h_pt));

        return new Trace(depart, "e");
    }

    /**
	*  Méthode pour afficher sur la console la trace du joueurs.
	*
	*	@param j: Le joueur auquel on veut afficher la trace.
	*/
    public static void printTrace(Joueur j) 
    {
    	Trace t = j.getT();
    	ListeChainee l = t.get_liste();
    	
    	for(int i =0; i < l.size(); i++)
    	{
    		System.out.println(l.get(i));
    	}
    }
}
