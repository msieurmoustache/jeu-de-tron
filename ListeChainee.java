/**
 * Fichier ListeChainee.java
 * Date: 31 Mars 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe ListeChainee crée la structure de donnée: la liste chaînée.
 */
public class ListeChainee implements Liste
{
    /**
     * First: Le premier noeud de la liste
     * Last:  Le dernier noeud de la liste
     * nb_elements: Nombre d'éléments que contient la liste
     */
    private Noeud first, last;
    private int nb_elements;

    /**
     * Constructeur de ListeChainee
     *  Initialise à zéro le nombre d'éléments de la liste.
     */
    public ListeChainee()
    {
        nb_elements = 0;
    }

    /**
     * Override de la méthode toString pour l'affichage de ListeChainee.
     * @return: String qui affichera les éléments de liste.
     */
    public String toString()
    {
        String s = "ListeChainée[";
        Noeud n = first;

        for(int i = 0; i < this.size(); i++)
        {

            if(n != null )
            {
                s += n.getContenu()+",";
                n = n.getNext();
            }
        }
        s = s.substring(0,s.length()-1);
        s += "]";

        return s;
    }


    /**
     * Méthode premier_noeud
     * Vérifie si c'est le premier noeud de la liste.
     * Si oui, il devient le first et last et retourne true.
     * Si non, retourne false.
     *
     * @param n
     * @return
     */
    public Boolean premier_noeud(Noeud n)
    {
        boolean a = false;
        if (nb_elements == 0)
        {
            first = n;
            last = n;
            a = true;

        }
        return a;
    }



    /**
     *  Méthodes de l'interface
     *    Voir Liste.java
     */
    public int size()
    {
        return nb_elements;
    }


    public void append(Object o)
    {

        Noeud n = new Noeud(o);
        if(!this.premier_noeud(n))
        {
            last.setNext(n);
            last = n;
        }
        nb_elements++;
    }


    public void preprend(Object o)
    {
        Noeud n = new Noeud(o);
        if(!this.premier_noeud(n))
        {
            n.setNext(first);
            first = n;
        }
        nb_elements++;

    }

    public Object getFirst()
    {
        if(nb_elements == 0) throw new IndexOutOfBoundsException("Liste vide");
        Object a = first.getContenu();
        return a;
    }

    public Object getLast()
    {
        if(nb_elements == 0) throw new IndexOutOfBoundsException("Liste vide");
        Object a =  last.getContenu();
        return a;
    }

    public Object removeFirst()
    {
        Object a = first.getContenu();
        Noeud n = first.getNext();
        first = n;
        nb_elements--;
        return a;
    }


    public Object get(int position)
    {
        if(position < 0 || position >= this.size()) throw new IndexOutOfBoundsException("Position hors de la liste ");

        Noeud n = first;

        for(int i = 0 ; i < position; i++ )
        {
            n = n.getNext();

        }
        return n.getContenu();
    }

    public void set(int position, Object o)
    {
        if(position < 0 || position >= this.size()) throw new IndexOutOfBoundsException("Position hors de la liste ");

        Noeud n = new Noeud(o);
        Noeud upper_noeud = first;
        for(int i = 0 ; i <= position-1; i++ )
        {
            upper_noeud = upper_noeud.getNext();
        }
        Noeud reset_noeud = upper_noeud.getNext();
        upper_noeud.setNext(n);
        n.setNext(reset_noeud.getNext());

    }

}
