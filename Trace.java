/**
 * Fichier Trace.java
 * Date: 26 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe Trace: Classe qui crée l'objet Trace avec l'aide de la classe Segment.
 * Consiste à être une liste chaînée de segment.
 */
public class Trace 
{
  private Liste segments;
  
  /**
  * Constructeur de Trace.
  *	Initialise l'attribut comme étant une liste vide.
  *
  */
  public Trace()
  {
    segments = new ListeChainee();
  }
  
  /**
  *	Autre constructeur de Trace.
  *	Initialise l'attribut segments avec la liste chaînée passé en paramètre.
  *
  *	@param l: ListeChainee
  */
  public Trace(ListeChainee l)
  {
	  segments = l;
  }

  /**
  *	Autre constructeur de Trace
  *	Initialise l'attribut segments en créant un segment avec les paramètres données.
  *
  *	@param depart: Point de départ pour le premier segment de segments	
  *	@param o: String indiquant l'orientation de ce segment.
  */
  public Trace(Point depart, String o)
  {
    segments = new ListeChainee();
    segments.append(new Segment(depart, o));
  }
  
  /**
   *  Méthode qui permet de mettre un nouveau segment à la fin de la Trace
   *
   *  @param seg: Segment à ajouter.
   */
  public void ajoute(Segment seg)
  {
    segments.append(seg);
  }
  
  /**
  * Méthode qui retourne l'attribut segment.
  *
  */
  public ListeChainee get_liste()
  {
    return (ListeChainee) segments;
  }

  
  /**
   *  Méthode qui augmente la longueur de la trace de l'en allongeant le dernier segment si la direction
   *  est la même ou en créant un nouveau segment si la direction est différente.
   *
   *  @param direction: String indiquant la direction.
   */
  public void allonge(String direction) 
  {
    boolean samedirection;

    Point raboute = ((Segment) segments.getLast()).getFin(); 
    if(direction == ((Segment) segments.getLast()).getOrientation()) samedirection = true; 
    else samedirection = false;

    switch(direction) 
    {
      case "n":  
        if (samedirection) raboute.setY(raboute.getY()-1);
        else 
        {
          segments.append(new Segment(raboute,"n"));
        }
        break;
      case "s":
        if (samedirection) raboute.setY(raboute.getY()+1);
        else 
        {
          segments.append(new Segment(raboute,"s"));
        }
        break;
      case "e":
        if (samedirection) raboute.setX(raboute.getX()+1);
        else 
        {
          segments.append(new Segment(raboute,"e"));
        }
        break;
      case "o":
        if (samedirection) raboute.setX(raboute.getX()-1);
        else 
        {
          segments.append(new Segment(raboute,"o"));
        }
        break;
    }
  }
  
  /**
   * Méthode qui retourne la tête de la trace, c'est-à-dire le dernier point du dernier segment.
   *
   */
  public Point tete() 
  {

    Segment bout = (Segment) segments.getLast(); 

    Point tete = bout.getFin();
    return tete;
  } 
  
  /**
  * Méthode qui vérifie si un point existe dans la trace.
  *
  * @param p: Point qui est regardé.
  */
  public boolean contient(Point p) 
  {
    Segment compare;
    for (int i = 0; i < segments.size(); i++)
    {
      compare = (Segment) segments.get(i); 

      Point debut = compare.getDebut();
      Point fin = compare.getFin();
      switch(compare.getOrientation()) 
      {
        case "n":
        case "s":
            if(p.getX() == debut.getX()) 
            {
                 if (debut.getY() <= fin.getY()) 
              {
                if (p.getY() >= debut.getY() & p.getY() <= fin.getY()) return true;

                else return false;

              }
              else 
              {
                if (p.getY() <= debut.getY() & p.getY() >= fin.getY()) return true;

                else return false;

              }
            }
          
        case "e":
        case "o":
            if(p.getY() == debut.getY()) {
                if (debut.getX() <= fin.getX()) 
              {
                if (p.getX() >= debut.getX() & p.getX() <= fin.getX()) return true;

                else return false;

              }
              else 
              {
                if (p.getX() <= debut.getX() & p.getX() >= fin.getX()) return true;

                else return false;

              }
            }
      }
    }
	return false;
  }
}