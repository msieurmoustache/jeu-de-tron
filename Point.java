/**
 * Fichier Point.java
 * Date: 26 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe Point: Classe crée l'objet point dont les coordonnées sont en deux dimensions.
 */
public class Point
{
    private int x, y;

	/**
	*	Constructeur de point.
	*	Initialise la composantes x et y du point.
	*
	*	@param nx: Coordonnée x
	*	@param ny: Coordonnée y
	*/
    public Point(int nx, int ny)
    {
        x = nx;
        y = ny;
    }
    
	/**
	*	Constructeur de Point qui prend en paramètre
	*	un point et en crée un nouveau avec les mêmes paramètres.
	*
	*	@param p: point.
	*/
    public Point(Point p)
    {
    	x = p.getX();
    	y = p.getY();
    }

	/**
	*	Méthode qui retourne ou modifie les coordonnées du point.
	*
	*
	*/
    public void setX(int nx) { x = nx; }
    public void setY(int ny) { y = ny; }
    public int getX()  { return x; }
    public int getY()  { return y; }

	/**
	*	Méthode qui retourne True si les deux points ont les mêmes coordonnées.
	*
	*	@param o: point à comparer.
	*/
    public boolean same(Point o)
    {
        boolean a = false;
        if(this.x == o.getX() && this.y == o.getY()) a = true;
        return a;
    }

}
