/**
 * Fichier EssaiListe.java
 * Date: 31 Mars 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe EssaiListe: Classe test des classes ListeChaînée et ListeTableau.
 */

public class EssaiListe
{
    public static void main(String[] args)
    {
        int chainee = Integer.parseInt(args[0]);
        int n = Integer.parseInt(args[1]);
        Liste l;
        if(chainee == 0) l = new ListeTableau();
        else if(chainee == 1) l = new ListeChainee();
        else throw new ArrayIndexOutOfBoundsException();

        testListe( (Liste) l, n);
    }

    public static void testListe(Liste l, int n)
    {
        System.out.println("append...");
        Integer app;
        for (int i = 1; i<=n;i++)
        {
            app = new Integer(i);
            l.append(app);
        }

        if(l.size() <= 100) System.out.println(l);
        System.out.println("prepend...");

        for (int i = 1; i<=n;i++)
        {
            app = new Integer(i);
            l.preprend(app);
        }

        if(l.size() <= 100) System.out.println(l);

        for (int i =0;i<n/2&&l.size()>0; i++)
        {
            l.removeFirst();
        }
        if(l.size() <= 100) System.out.println(l);
        System.out.println("taille="+l.size()+"  premier="+l.getFirst()+ "  dernier="+l.getLast() );
        System.out.println("Avant-dernier="+l.get(l.size()-2) );
        if(l.size() <= 100) System.out.println(l);
    }
}