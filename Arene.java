import javax.swing.*;
import java.awt.*;

/**
 * Fichier Arene.java
 * Date: 28 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe Arene: La composante graphique de l'Arène.
 */
public class Arene extends JComponent
{
	

    public int largeur_grille, hauteur_grille;
    public Joueur[] joueurs;
    public Trace enceinte;

	/**
	*	Méthode qui donne la couleur au joueur selon le numéro de celui-ci.
	*
	*	@param i: Numéro du personnage.
	*   @return: La couleur demandée.
	*/
    static Color choix_couleur(int i)
    {
    	switch(i) {
    	case 0: return Color.blue;
    	case 1: return Color.green;
		default: return Color.red;
    	}
    }
	
	/**
	*	Méthode qui crée la trace de l'enceinte. 
	*
	*	@param dim_largeur: Largeur de l'enceinte en pixel
	*   @param dim_hauteur: Hauteur de l'enceinte en pixel
	*   @return: la Trace de l'enceinte.
	*/
    private static Trace creation_Enceinte(int dim_largeur, int dim_hauteur)
    {
        Point haut_gauche = new Point(0,0);
        Point haut_droite = new Point(dim_largeur, 0);
        Point bas_droite = new Point(dim_largeur, dim_hauteur);
        Point bas_gauche = new Point(0, dim_hauteur);

        Segment haut = new Segment(haut_gauche,haut_droite);
        haut.setOrientation("e");
        Segment droite = new Segment(haut_droite,bas_droite);
        droite.setOrientation("n");
        Segment bas = new Segment(bas_droite, bas_gauche);
        bas.setOrientation("e");
        Segment gauche = new Segment(bas_gauche, haut_gauche);
        gauche.setOrientation("n");

        Trace t =  new Trace();

        t.ajoute(haut);
        t.ajoute(droite);
        t.ajoute(bas);
        t.ajoute(gauche);

        return t;
    }
	
	/**
	*
	*	Méthode qui s'assure que le point de départ des joueurs ne soient pas le même.
	*
	*	@param i: indice du joueur nouvellement créé.
	*	@param tab: Tableau des joueurs du jeu.
	*	
	*/	
    private void assure_difference(int i, Joueur[] tab)
    {
        if(i != 0)
        {
            for (int j = i-1; j>0; j--){
                Point pi = ((Segment)(((tab[i].getT()).get_liste()).getFirst())).getDebut();
                Point pj = ((Segment)(((tab[j].getT()).get_liste()).getFirst())).getDebut();
                if(pi.same(pj))
                {
                    tab[i].nouvelle_Trace(this.largeur_grille/20, this.hauteur_grille/20);
                    assure_difference(i,tab);
                }
            }
        }
    }

	/**
	*	Constructeur de l'arène par défault
	*	1 ordinateur et 1 humain.
	*
	**/
    public Arene() 
	{
		largeur_grille = 500;
		hauteur_grille = 500;
		joueurs = new Joueur[2];

		joueurs[0] = new JoueurHumain(Color.BLUE, largeur_grille/20, hauteur_grille/20);
		joueurs[1] = new JoueurOrdinateur(Color.RED, largeur_grille/20, hauteur_grille/20);
		assure_difference(1, joueurs);
		enceinte = creation_Enceinte(largeur_grille/20,hauteur_grille/20);
    
		Trace[] tab = new Trace[]{joueurs[0].getT(),joueurs[1].getT(),enceinte};
		((JoueurOrdinateur) joueurs[1]).set_tab_trace(tab);
	}
	
	/**
	* 	Deuxième constructeur de l'arène.
	*	Pour plus de 2 joueurs.
	*
	*	@param nbJoueurs: Nombre de joueurs présent dans la parti.
	*	@param ordi: Vrai quand on veut un joueur ordinateur.
	*/
    public Arene(int nbJoueurs, boolean ordi)
    {
    	    largeur_grille = 500;
    	    hauteur_grille = 500;
    	    if (ordi) {
    	    	joueurs = new Joueur[nbJoueurs+1];
    	    	joueurs[2] = new JoueurOrdinateur(Color.RED, largeur_grille/20, hauteur_grille/20);
    	    }
    	    else joueurs = new Joueur[nbJoueurs];

    	    joueurs[0] = new JoueurHumain(Color.BLUE, largeur_grille/20, hauteur_grille/20);
    	    joueurs[1] = new JoueurHumain(Color.GREEN, largeur_grille/20, hauteur_grille/20);
    	    assure_difference(1, joueurs);
    	    enceinte = creation_Enceinte(largeur_grille/20,hauteur_grille/20);
    	    
    	    
    	    if (ordi) {
    	    	Trace[] tab = new Trace[]{joueurs[0].getT(),joueurs[1].getT(),joueurs[2].getT(),enceinte};
        	    ((JoueurOrdinateur) joueurs[2]).set_tab_trace(tab);
    	    }
    	    
    }
	
	/**
	*	Redéfinition de paintComponent
	*	Dessine l'enceinte de l'arène ainsi que les traces des joueurs.
	*
	*/
    public void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        // Création de l'enceinte.
        for( int i = 0; i < (enceinte.get_liste()).size(); i++)
        {
          Segment seg =(Segment)(enceinte.get_liste()).get(i);
          Point dep = seg.getDebut();
          Point fin = seg.getFin();
          g.drawLine(dep.getX()*20+20,dep.getY()*20+20, fin.getX()*20+20, fin.getY()*20+20);
        }

        //Tracer des traces
        for(int i = 0; i < joueurs.length; i++)
        {
            Trace t = joueurs[i].getT();
            g.setColor(joueurs[i].getCouleur());
            for( int j = 0; j < (t.get_liste()).size(); j++)
            {
                Segment seg =(Segment)(t.get_liste()).get(j);
                Point dep = seg.getDebut();
                Point fin = seg.getFin();
                g.drawLine(dep.getX()*20+20,dep.getY()*20+20, fin.getX()*20+20, fin.getY()*20+20);
            }

        }


    }
    
    
	/**
	*	Méthode qui retourne le tableau joueurs
	*
	*/    
    public Joueur[] getJoueur()
	{
		return joueurs;
	}


}
