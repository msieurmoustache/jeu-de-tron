/**
 * Fichier Joueur.java
 * Date: 28 avril 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe JoueurOrdinateur: Classe crée le joueur ordinateur et fait se déplacer aléatoirement.
 */
import java.awt.*;
import java.util.Random;
public class JoueurOrdinateur extends Joueur 
{
	
	private Trace[] tab_trace;
	/**
	* Constructeur de JoueurOrdinateur.
	* Ce qui n'est que le constructeur ordinaire de Joueur
	*
	*/
	public JoueurOrdinateur(Color c, int dim_h, int dim_larg) {
        super(c, dim_h, dim_larg);
    }
	
	/**
	*	Méthode set_tab_trace est la méthode qui initialise le tableau de trace 
	*	qui permet au joueur ordinateur d'avoir l'information de ce qui l'entoure.
	*
	*	@param tab: Tableau de Trace.
	*/
    public void set_tab_trace(Trace[] tab)
    {
    	tab_trace = tab;
    }
    
	/**
	*	Méthode qui vérifie la présence du point dans le tableau de trace.
	*
	*	@param p: Point recherché.
	*
	*/
    public boolean presente(Point p)
    {
    	for(int i = 0; i < tab_trace.length ; i++)
    	{
    		if(tab_trace[i].contient(p)) return true;
    	}
    	return false;
    	
    }
    
	/**
	*	Méthode qui donne le point suivant à la tête
	*
	*	@param tete: Point qui indique où est la tête de la trace du joueur.
	*	@param o: String représentant l'orientation  pour savoir où sera le point suivant de la trace.
	*	
	*/
    public Point suivant_test(Point tete,String o)
    {
    	switch(o)
    	{
    		case "n": return new Point(tete.getX(),tete.getY()-1);
    		case "s": return new Point(tete.getX(),tete.getY()+1);
    		case "o":  return new Point(tete.getX()-1,tete.getY());
    		case "e":return new Point(tete.getX()+1,tete.getY());
    		default: return new Point(tete);
    	}
    }
    
	/**
	*	Override de la méthode deplacement() de Joueur.
	*	Fait les changements de direction et allonge la trace.
	*	Regarde si le point suivant le déplacement est un point avec une trace ou avec l'enceinte
	*	avec les méthode suivant_test() et presente().
	*	S'il s'avère que le joueur risque de frapper une trace, il change aléatoirement de direction.
	*	On s'assure qu'il ne retourne pas sur son chemin et où qu'il ne retourne pas dans la même direction.
	*/
	
	
    public void deplacement()
    {
    	if( nouvelle_direction != direction_courante)
    	{
    		direction_courante = nouvelle_direction;
    		t.allonge(direction_courante);
    	}
    	else{
    		t.allonge(direction_courante);
    		Point suivant = suivant_test(t.tete(),direction_courante);
    		
    		if(presente(suivant))
    		{
    			Random rand = new Random();
    			boolean a = true;
    			while(a)
    			{
    				int o = rand.nextInt(4);
    				switch(o)
    				{
	    				case 0:
	    					if(this.direction_courante == "n" || this.direction_courante == "s") continue;
	    					else
	    					{
	    						a = false;
	    						nouvelle_direction = "n";
	    					}
	    					break;
	    				case 1: 
	    					if(this.direction_courante == "s" || this.direction_courante == "n") continue;
	    					else
	    					{
	    						a = false;
	    						nouvelle_direction = "s";
	    					}
	    					break;
	    				case 2: 
	    					if(this.direction_courante == "o" || this.direction_courante == "e") continue;
	    					else
	    					{
	    						a = false;
	    						nouvelle_direction = "o";
	    					}
	    					break;
	    				case 3:  
	    					if(this.direction_courante == "e" || this.direction_courante == "o") continue;
	    					else
	    					{
	    						a = false;
	    						nouvelle_direction = "e";
	    					}
	    					break;
    				}
    			}
    		}
    	}
    }    
}
